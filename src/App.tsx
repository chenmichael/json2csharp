import { useState } from "react";
import "./App.css";
import { getType, isKnownType } from "./JsonType";
import { Language } from "./Language";
import { emitter as cSharpEmitter } from "./SchemaEmitter/CSharpEmitter";
import { emitter as pythonEmitter } from "./SchemaEmitter/PythonEmitter";

const emitters = {
  [Language.CSharp]: cSharpEmitter,
  [Language.Python]: pythonEmitter,
} as const;

function createModel(text: string, language: Language, name: string) {
  const emitter = emitters[language];
  try {
    const parsed = JSON.parse(text);
    const type = getType(parsed);
    if (!isKnownType(type)) throw new Error(`Resulting type is unknown!`);
    return emitter.emit(type, name);
  } catch (error) {
    if (!(error instanceof SyntaxError)) console.error(error);
    return error instanceof Error
      ? `Error: ${error.message}`
      : `Unknown error: ${error}`;
  }
}

function App() {
  const [text, setText] = useState("");
  const [name, setName] = useState("Root");
  const [language, setLanguage] = useState(Language.CSharp);

  return (
    <div className="app">
      <input value={name} onChange={(e) => setName(e.currentTarget.value)} />
      <select
        value={language}
        onChange={(e) => setLanguage(Number.parseInt(e.currentTarget.value))}
      >
        <option value={Language.CSharp}>C#</option>
        <option value={Language.Python}>Python</option>
      </select>
      <textarea onChange={(e) => setText(e.currentTarget.value)} />
      <textarea value={createModel(text, language, name)} readOnly />
    </div>
  );
}

export default App;
