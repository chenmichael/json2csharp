export interface JsonFieldType {
  type: JsonType;
  required: boolean;
}

export interface JsonComplexFields {
  [key: string]: JsonFieldType | undefined;
}

export interface JsonComplexType {
  type: "complex";
  fields: JsonComplexFields;
}

export const namedTypeNames = [
  "int",
  "double",
  "string",
  "bool",
  "DateTime",
  "DateOnly",
] as const;

export const namedTypes = namedTypeNames.map(makeNamedType);

export type NamedTypeName = (typeof namedTypeNames)[number];

export interface JsonNamedType {
  type: "named";
  name: NamedTypeName;
}

export function makeNamedType(name: NamedTypeName): JsonNamedType {
  return { type: "named", name };
}

export interface JsonArrayType {
  type: "array";
  items: JsonType;
}

export interface JsonUnknownType {
  type: "unknown";
}

const UnknownType: JsonUnknownType = { type: "unknown" };

export interface JsonNullableType {
  type: "nullable";
  wrapped: JsonNonNullType;
}

export function makeNullable(type: JsonType): JsonNullableType {
  return {
    type: "nullable",
    wrapped: unwrapNullable(type),
  };
}

export type JsonType =
  | JsonNullableType
  | JsonComplexType
  | JsonArrayType
  | JsonNamedType
  | JsonUnknownType;

export type JsonNonNullType = Exclude<JsonType, JsonNullableType>;
export type JsonKnownType = Exclude<JsonType, JsonUnknownType>;

export function isNamedSubtypeOf(left: JsonNamedType, right: JsonNamedType) {
  if (left.name === right.name) return true;
  if (left.name === "int" && right.name === "double") return true;
  if (left.name === "DateTime" && right.name === "string") return true;
  if (left.name === "DateOnly" && right.name === "string") return true;
  return false;
}

export function isArraySubtypeOf(left: JsonArrayType, right: JsonArrayType) {
  return isSubtypeOf(left.items, right.items);
}

export function isSubtypeOf(left: JsonType, right: JsonType): boolean {
  if (left.type !== right.type) return false;
  switch (left.type) {
    case "named":
      return isNamedSubtypeOf(left, right as JsonNamedType);
    case "array":
      return isArraySubtypeOf(left, right as JsonArrayType);
    default:
      throw new Error(
        `isSubtypeOf<${left.type}, ${right.type}> not implemented!`
      );
  }
}

export function areSubtypesOf(types: JsonType[], right: JsonType) {
  return types.every((type) => isSubtypeOf(type, right));
}

export function isDefined<T>(item: T | undefined): item is T {
  return !!item;
}

export function isType<T extends JsonType["type"]>(metaType: T) {
  return (type: JsonType): type is Extract<JsonType, { type: T }> =>
    type.type === metaType;
}

export function isNotType<T extends JsonType["type"]>(metaType: T) {
  const isThisType = isType(metaType);
  return (
    type: JsonType
  ): type is Exclude<JsonType, Extract<JsonType, { type: T }>> =>
    !isThisType(type);
}

export const isNullable = isType("nullable");
export const isKnownType = isNotType("unknown");

export function areType<T extends JsonType["type"]>(
  metaType: T,
  types: JsonType[]
): types is Extract<JsonType, { type: T }>[] {
  return types.every(isType(metaType));
}

export function assertAreTypes<T extends JsonType["type"]>(
  metaType: T,
  types: JsonType[]
) {
  if (!areType(metaType, types)) throw new Error("Expected named types");
  return types;
}

export function unwrapNullable(type: JsonType): JsonNonNullType {
  return type.type === "nullable" ? type.wrapped : type;
}

export function mergeNamedTypes(types: JsonNamedType[], name: string) {
  const typeNames = [...new Set(types.map((type) => type.name))];
  const uniqueTypes = typeNames.map(makeNamedType);
  if (uniqueTypes.length === 1) return uniqueTypes[0];
  for (const parent of uniqueTypes)
    if (areSubtypesOf(uniqueTypes, parent)) return parent;
  for (const parent of namedTypes)
    if (areSubtypesOf(uniqueTypes, parent)) return parent;
  throw new Error(`Cannot merge types ${typeNames} at '${name}'`);
}

export function getMetaTypes<T extends JsonType>(
  types: T[]
): JsonType["type"][] {
  return [...new Set(types.map((type) => type.type))];
}

export function mergeComplexTypes(
  types: JsonComplexType[],
  name: string
): JsonComplexType {
  const keys = [...new Set(types.flatMap((type) => Object.keys(type.fields)))];
  return {
    type: "complex",
    fields: Object.fromEntries(
      keys.map((key) => {
        const keyFields = types.map((type) => type.fields[key]);
        const definedKeyFields = keyFields.filter(isDefined);
        const required =
          definedKeyFields.length === keyFields.length &&
          definedKeyFields.every((keyType) => keyType.required);
        const definedFieldTypes = definedKeyFields.map((field) => field.type);
        return [
          key,
          { type: mergeTypes(definedFieldTypes, `${name}.${key}`), required },
        ];
      })
    ),
  };
}

export function mergeArrayTypes(
  types: JsonArrayType[],
  name: string
): JsonArrayType {
  return {
    type: "array",
    items: mergeTypes(
      types.map((type) => type.items),
      `${name}.[x]`
    ),
  };
}

export function mergeTypes(types: JsonType[], name: string): JsonType {
  if (types.length === 0) return UnknownType;
  if (types.length === 1) return types[0];
  if (types.some(isNullable))
    return makeNullable(mergeTypes(types.map(unwrapNullable), name));
  types = types.filter<JsonKnownType>(isKnownType);
  const metaTypes = getMetaTypes(types);
  if (metaTypes.length === 0) return UnknownType;
  if (metaTypes.length !== 1)
    throw new Error(
      `Cannot merge different meta types ${metaTypes} at '${name}'!`
    );
  const metaType = metaTypes[0];
  switch (metaType) {
    case "named":
      return mergeNamedTypes(assertAreTypes("named", types), name);
    case "complex":
      return mergeComplexTypes(assertAreTypes("complex", types), name);
    case "array":
      return mergeArrayTypes(assertAreTypes("array", types), name);
    default:
      throw new Error(`mergeTypes<${metaType}> not implemented at '${name}'!`);
  }
}

type JsonParsedType =
  | JsonParsedType[]
  | { [key: string]: JsonParsedType | undefined }
  | number
  | string
  | boolean
  | null;

export function getType(
  value: JsonParsedType | null | undefined,
  name: string = "$"
): JsonType {
  if (value === undefined)
    throw new Error(`Cannot get type of undefined value at '${name}'!`);
  if (value === null) return makeNullable(UnknownType);
  switch (typeof value) {
    case "object":
      if (Array.isArray(value)) {
        return {
          type: "array",
          items: mergeTypes(
            value.map((item, index) => getType(item, `${name}.[${index}]`)),
            `${name}.[x]`
          ),
        };
      } else {
        return {
          type: "complex",
          fields: Object.fromEntries(
            Object.keys(value).map((key) => [
              key,
              {
                type: getType(value[key], `${name}.${key}`),
                required: true,
              },
            ])
          ),
        };
      }
    case "number":
      return {
        type: "named",
        name: Number.isInteger(value) ? "int" : "double",
      };
    case "string":
      try {
        const isoString = new Date(value).toISOString();
        if (isoString === value) return { type: "named", name: "DateTime" };
        if (isoString.slice(0, 10) === value)
          return { type: "named", name: "DateOnly" };
      } catch {
        // ignore failure and return default string
      }
      return { type: "named", name: "string" };
    case "boolean":
      return { type: "named", name: "bool" };
    default:
      throw new Error(`Cannot get type of ${typeof value} at ${name}!`);
  }
}
