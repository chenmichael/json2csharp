import {
  JsonArrayType,
  JsonComplexType,
  JsonNullableType,
  JsonType,
  mergeTypes,
} from "../JsonType";
import { singular } from "../singular";
import { Class, stringifyClass } from "./EmitterHelper";
import { SchemaEmitter } from "./SchemaEmitter";
import { toPascalCase } from "js-convert-case";

const numberRegex = /^\d+$/;

function createComplexClass(type: JsonComplexType, name: string = "Root") {
  if (Object.keys(type.fields).every((i) => numberRegex.test(i))) {
    const values = Object.values(type.fields)
      .filter((i) => i !== undefined)
      .map((i) => i.type);
    const valueType = mergeTypes(values, `${name}.[x]`);
    const valueClass = createClass(valueType, `${name}Item`);
    return {
      name: `IReadOnlyDictionary<int, ${valueClass.name}>`,
      dependencies: [valueClass],
    } satisfies Class;
  }
  let code = `public class ${name} {`;
  const fields = type.fields;
  const dependencies: Class[] = [];
  for (const fieldName in fields) {
    const field = fields[fieldName];
    if (field === undefined)
      throw new Error(
        `In class ${name}, the field ${fieldName} has no defined type!`
      );
    const friendlyName = toPascalCase(fieldName);
    const fieldClass = createClass(field.type, `${name}${friendlyName}`);
    const typeString = fieldClass.name;
    dependencies.push(fieldClass);
    code += `\n    [JsonPropertyName("${fieldName}")] public${
      field.required ? " required" : ""
    } ${typeString} ${friendlyName} { get; set; }`;
  }
  code += `\n}\n`;
  return { name, code, dependencies };
}

function createArrayClass(type: JsonArrayType, name: string = "Root") {
  const itemClass = createClass(type.items, singular(name));
  return {
    name: `IReadOnlyList<${itemClass.name}>`,
    dependencies: [itemClass],
  };
}

function createNullableClass(type: JsonNullableType, name: string = "Root") {
  const nonNullClass = createClass(type.wrapped, name);
  return { name: `${nonNullClass.name}?`, dependencies: [nonNullClass] };
}

function createClass(type: JsonType, name: string = "Root"): Class {
  switch (type.type) {
    case "complex":
      return createComplexClass(type, name);
    case "named":
      return { name: type.name };
    case "nullable":
      return createNullableClass(type, name);
    case "array":
      return createArrayClass(type, name);
    case "unknown":
      console.warn(`Handling unknown type for ${name} as object.`);
      return { name: "object" };
  }
}

export const emitter: SchemaEmitter = {
  emit: (type, name?: string) => stringifyClass(createClass(type, name)),
};
