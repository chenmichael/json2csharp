export interface Class {
  name: string;
  code?: string;
  dependencies?: Class[];
}

export function* iterateDependencies(classes: Class): Generator<string> {
  const code = classes.code;
  const dependencies = classes.dependencies;
  if (dependencies !== undefined)
    for (const subClass of dependencies) yield* iterateDependencies(subClass);
  if (code !== undefined) yield code;
}

export function stringifyClass(classes: Class) {
  let output = "";
  let first = true;
  for (const subClass of iterateDependencies(classes)) {
    if (first) first = false;
    else output += "\n";
    output += subClass;
  }
  if (classes.code === undefined) output += `\n// Model: ${classes.name}`;
  return output;
}
