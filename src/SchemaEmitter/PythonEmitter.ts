import { SchemaEmitter } from "./SchemaEmitter";

export const emitter: SchemaEmitter = {
  emit: (type) => {
    throw new Error(`Python emission for type ${type.type} not implemented!`);
  },
};
