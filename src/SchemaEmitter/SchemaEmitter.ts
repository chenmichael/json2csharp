import { JsonKnownType } from "../JsonType";

export interface SchemaEmitter {
  emit: (type: JsonKnownType, name: string) => string;
}
