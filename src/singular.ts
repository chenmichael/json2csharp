const suffixes = [
  ["ch", "ches"],
  ["sh", "shes"],
  ["s", "ses"],
  ["x", "xes"],
  ["z", "zes"],
  ["", "s"],
] as const;

const vowels = new Set("aeiou");

function isVowel(character: string) {
  return vowels.has(character);
}

function isConsonant(character: string) {
  return !isVowel(character);
}

export function singular(plural: string) {
  if (plural.endsWith("ies")) {
    const stem = plural.slice(0, plural.length - 3);
    if (stem.length > 0) {
      if (isConsonant(stem[stem.length - 1])) return `${stem}y`;
    }
  }
  for (const [sing, plur] of suffixes)
    if (plural.endsWith(plur))
      return `${plural.slice(0, plural.length - plur.length)}${sing}`;
  console.warn(`No known plural form for '${plural}'!`);
  return plural;
}
